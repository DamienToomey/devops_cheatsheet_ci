FROM ubuntu
ADD python_app/src /app/python_app/src
# copy content of python_app/src to /app/python_app/src
WORKDIR /app/python_app/src
RUN apt-get update -yq \
    && apt-get install python3-dev -yq
CMD python3 app.py
#CMD echo $(ls)