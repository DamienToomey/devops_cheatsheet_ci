### Install `pytest`

```
pip3 install pytest
```

### Run `pytest`

```
python3 -m pytest -s -v
# python3 to make sure we use pytest with python3
# -s: shortcut for --capture=no. (show print output in terminal)
# -v, --verbose: increase verbosity. (display name of functions being tested)
```

### Install `pytest`

```
pip3 install pylint
```

### Run `pylint`

```
python3 -m pylint app.py
# python3 to make sure we use pylint with python3
```

```
python3 -m pylint unit_test.py
# python3 to make sure we use pylint with python3
```
